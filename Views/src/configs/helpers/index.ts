import GoalStatus from "enums/GoalStatus";

export function convertStatusToString(n: number | string) {
  switch (n) {
    case 0: return "Pendente";
    case 1 : return "Finalizado";
    default: return undefined;
  }
}


export function convertStatusToEnum(n: number | string) {
  switch (n) {
    case 0 : return  GoalStatus.Pendente;
    case 1 : return  GoalStatus.Finalizado;
    default: return GoalStatus.Pendente;
  }
}

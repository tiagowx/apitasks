import { configureStore } from "@reduxjs/toolkit";
import goalSlice from "./reducers/goalSlice";

const store = configureStore({
  reducer: 
    goalSlice
  
});

export default store;
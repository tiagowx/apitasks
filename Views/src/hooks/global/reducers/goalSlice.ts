
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import GoalStatus from "enums/GoalStatus";
import IGoal from "interfaces/IGoal";
import ServiceTasks from "services/ServiceTasks";

const goalService = new ServiceTasks();

export const createGoal = createAsyncThunk('goals/createOne',
  async (goal: IGoal) => {
    const result = await goalService.createGoal(goal);
    return result;
  });

export const getAllGoals = createAsyncThunk('goals/getAll', async () => {
  return goalService.getGoalsAll();
});

export const getByFilters = createAsyncThunk('goals/getByFilters', async (goal: IGoal) => {
  return goalService.getGoalsByFilters(goal);
});

export const getByDate = createAsyncThunk('goals/getByDate', async (date: Date) => {
  return goalService.getGoalsByDate(date);
});

export const getByDescription = createAsyncThunk('goals/getByDescription', async (description: string) => {
  return goalService.getGoalsByDescription(description);
});

export const getByStatus = createAsyncThunk('goals/getByStatus', async (status: GoalStatus) => {
  return goalService.getGoalsByStatus(status);
});

export const getByTitle = createAsyncThunk('goals/getByTitle', async (title: string) => {
  return goalService.getGoalsByTitle(title);
});

export const update = createAsyncThunk('goals/update', async (goal: IGoal) => {
  return goalService.updateGoal(goal);
});

export const deleteById = createAsyncThunk('goals/deleteById', async (id: number) => {
  return goalService.deleteGoalById(id);
});


export const goalSlice = createSlice({
  name: 'goals',
  initialState: {
    goals: [] as IGoal[]
  },
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(createGoal.fulfilled, (state, action) => { })
      .addCase(getAllGoals.fulfilled, (state, action) => {
        state.goals = action.payload;
      })
      .addCase(getByFilters.fulfilled, (state, action) => {
        state.goals = action.payload
      })
      .addCase(getByDate.fulfilled, (state, action) => {
        state.goals = action.payload
      })
      ;
  }
});


export const selectAllGoals = (state: { goals: IGoal[]; }) => state.goals



export default goalSlice.reducer;



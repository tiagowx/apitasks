import * as Yup from "yup";
import GoalStatus from "enums/GoalStatus";

const goalFormSchema = Yup.object().shape({
  title: Yup.string()
    .required('Você precisa dar um título a sua tarefa')
    .min(3, 'Não contem a quantidade mímina de caracteres.')
    .max(32, 'Execeu a quantidade máxima de caracteres.'),
  description: Yup.string()
    .required('Você precisa descrever sua tarefa')
    .min(3, 'Não contem a quantidade mímina de caracteres.')
    .max(255, 'Execeu a quantidade máxima de caracteres.'),
  date: Yup.date()
    .required('Você precisa indicar a data da realização da tarefa.')
    .min(0, 'Data inserida não é válida.'),
  status: Yup.mixed().oneOf(Object.values(GoalStatus))
    .required('Você precisa selecionar o status atual da tarefa')
});

export default goalFormSchema;
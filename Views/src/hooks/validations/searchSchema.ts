import { Fragment } from "react";
import {useForm, Controller} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { YoutubeSearchedForSharp } from "@mui/icons-material";
import GoalStatus from "enums/GoalStatus";

const searchSchema = Yup.object().shape({
  search: Yup.string()
    .required()
    .min(1, 'Campo vazio'),
});

export default searchSchema;
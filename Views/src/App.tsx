import './App.css';
import { Provider } from 'react-redux';
import Pages from './pages';
import store from 'global/store';
import { ThemeProvider } from '@emotion/react';
import defaultTheme from 'pages/Themes/defaultTheme';

function App() {

  return (
    <Provider store={store}>
      <ThemeProvider theme={defaultTheme}>
        <Pages />
      </ThemeProvider>
    </Provider>
  );
}

export default App;

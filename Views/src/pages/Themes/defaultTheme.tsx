import { createTheme } from "@mui/material";
import * as locales from "@mui/material/locale"

type SupportedLocales = keyof typeof locales;

const locale: SupportedLocales = 'ptBR';

const defaultTheme = createTheme(
  {},
  locales[locale]
);

export default defaultTheme;
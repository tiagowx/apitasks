import { Box, Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material";
import GoalStatus from "enums/GoalStatus";
import IGoal from "interfaces/IGoal";
import DeleteGoalModal from "../DeleteGoalModal";
import UpdateGoalModal from "../UpdateGoalModal";

interface Props {
  goals: IGoal[];
}

const GoalsTable: React.FC<Props> = (props: Props) => {

  return (
    <Box sx={{
        display:'flex',
        maxHeight: '440px',
        overflowY: "auto"
      }}>
      <Table >
        <TableHead>
          <TableRow >
            <TableCell sx={{ fontWeight: "bold" }}>#</TableCell>
            <TableCell sx={{ fontWeight: "bold" }}>Título</TableCell>
            <TableCell sx={{ fontWeight: "bold" }}>Descrição</TableCell>
            <TableCell sx={{ fontWeight: "bold" }}>Data</TableCell>
            <TableCell sx={{ fontWeight: "bold" }}>Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.goals && props.goals.map((item, index) =>
            <TableRow key={`goal-row-${index}`}>
              <TableCell >{index + 1}</TableCell>
              <TableCell>{item.title}</TableCell>
              <TableCell>{item.description}</TableCell>
              <TableCell>{new Date(item?.date || "").toLocaleDateString("pt-br")}</TableCell>
              <TableCell>{Object.values(GoalStatus)[item.status]}</TableCell>
              <TableCell sx={{
                display: 'flex',
                gap: '4px'
              }}>
                <UpdateGoalModal goal={item} />
                <DeleteGoalModal goal={item} />
              </TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </Box>
  );
};

export default GoalsTable;
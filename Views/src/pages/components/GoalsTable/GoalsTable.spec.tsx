import { render, screen } from '@testing-library/react';
import IGoal from 'interfaces/IGoal';
import MockGoalsTable from 'mocks/GoalsTable';
import GoalsTable from '.';

const mockGoal = {
  id: jest.fn(),
  title: jest.fn(),
  description: jest.fn(),
  date: jest.fn(),

}

const tableMock = MockGoalsTable;

describe('GoalsTable component test', () => {
  
  test('renders empty', () => {
    // render
    render(<GoalsTable goals={[]}/>)    
  });
  test('renders without crashing all the cells', () => {
    // render
    render(<GoalsTable goals={tableMock}/>)

    // set query
    const id = screen.getByText("1");
    const description = screen.getByText(tableMock[0].description);
    const date = screen.getByText("15/11/2022");
    const title = screen.getByText(tableMock[0].title);
    const status = screen.getByText(tableMock[0].status);
    const btnEdit = screen.getByTestId("btn-edit");
    const btnDelete = screen.getByTestId("btn-delete");

    // expect
    expect(id).toBeInTheDocument();
    expect(description).toBeInTheDocument();
    expect(date).toBeInTheDocument();
    expect(title).toBeInTheDocument();
    expect(status).toBeInTheDocument();
  })
});
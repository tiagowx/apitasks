import { Button, createMuiTheme, createStyles, Grid, makeStyles, TextField, ThemeProvider } from '@mui/material';
import defaultTheme from 'pages/Themes/defaultTheme';
import React, { useState } from 'react';





interface ServiceOrder {
  name: string;
  address: string;
  phoneNumber: string;
  email: string;
  problemDescription: string;
  date: string;
  time: string;
}

const ServiceOrderForm: React.FC = () => {
  const classes = defaultTheme;
  const [serviceOrder, setServiceOrder] = useState<ServiceOrder>({
    name: '',
    address: '',
    phoneNumber: '',
    email: '',
    problemDescription: '',
    date: '',
    time: '',
  });

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    // Enviar dados do formulário para o banco de dados ou para o servidor aqui
    console.log(serviceOrder);
  };

  return (
    <form  noValidate autoComplete="off" onSubmit={handleSubmit}>
      <Grid container spacing={3} sx={{display:'flex'}}>
        <Grid item xs={12}>
          <TextField
            required
            id="name"
            label="Nome"
            value={serviceOrder.name}
            onChange={(event) =>
              setServiceOrder({ ...serviceOrder, name: event.target.value })
            }
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="address"
            label="Endereço"
            value={serviceOrder.address}
            onChange={(event) =>
              setServiceOrder({ ...serviceOrder, address: event.target.value })
            }
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="phoneNumber"
            label="Telefone"
            value={serviceOrder.phoneNumber}
            onChange={(event) =>
              setServiceOrder({
                ...serviceOrder,
                phoneNumber: event.target.value,
              })
            }
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="email"
            label="E-mail"
            value={serviceOrder.email}
            onChange={(event) =>
              setServiceOrder({ ...serviceOrder, email: event.target.value })
            }
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="problemDescription"
            label="Descrição do problema"
            value={serviceOrder.problemDescription}
            onChange={(event) =>
              setServiceOrder({
                ...serviceOrder,
                problemDescription: event.target.value,
              })
            }
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="date"
            label="Data desejada"
            type="date"
            value={serviceOrder.date}
            onChange={(event) =>
              setServiceOrder({ ...serviceOrder, date: event.target.value })
            }
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="time"
            label="Hora desejada"
            type="time"
            value={serviceOrder.time}
            onChange={(event) =>
              setServiceOrder({ ...serviceOrder, time: event.target.value })
            }
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              step: 300, // 5 minutos
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <Button type="submit" variant="contained" color="primary">
            Enviar
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default ServiceOrderForm;
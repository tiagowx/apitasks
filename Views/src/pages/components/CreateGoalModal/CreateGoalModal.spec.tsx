import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import CreateGoalModal from '.';

describe('Form component', () => {
  test('renders input fields and submit button', () => {
    const { getByLabelText, getByText } = render(<CreateGoalModal />);
    const titleInput = getByLabelText('Título');
    const descriptionInput = getByLabelText('Descrição');
    const dateInput = getByLabelText('Data');
    const statusInput = getByLabelText('Status');
    const submitButton = getByText('Criar');

    expect(titleInput).toBeInTheDocument();
    expect(descriptionInput).toBeInTheDocument();
    expect(dateInput).toBeInTheDocument();
    expect(statusInput).toBeInTheDocument();
    expect(submitButton).toBeInTheDocument();
  });

  // test('submitting the form calls the onSubmit prop with the input values', () => {
  //   const onSubmit = jest.fn();
  //   const { getByLabelText, getByText } = render(<Form onSubmit={onSubmit} />);
  //   const nameInput = getByLabelText('Name') as HTMLInputElement;
  //   const emailInput = getByLabelText('Email') as HTMLInputElement;
  //   const submitButton = getByText('Submit');

  //   fireEvent.change(nameInput, { target: { value: 'John Doe' } });
  //   fireEvent.change(emailInput, { target: { value: 'john@example.com' } });
  //   fireEvent.click(submitButton);

  //   expect(onSubmit).toHaveBeenCalledWith({
  //     name: 'John Doe',
  //     email: 'john@example.com',
  //   });
  // });
});

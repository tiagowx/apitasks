import { render, screen } from '@testing-library/react';
import IGoal from 'interfaces/IGoal';
import DeleteGoalModal from '.';


describe('GoalsTable component test', () => {

  const goal: IGoal = {
    id: 0,
    date: new Date(0),
    status: 0,
    description: "descriçao",
    title: "titulo"
  }

  test('renders empty', () => {
    // render
    render(<DeleteGoalModal goal={goal} />)
  });

  test('renders without crashing', () => {
    // render
    render(<DeleteGoalModal goal={goal} />)

    // act

    // set query
    const button = screen.getByTestId("create-goal-modal-button");

    // expect
    expect(button).toBeInTheDocument();

  })
});
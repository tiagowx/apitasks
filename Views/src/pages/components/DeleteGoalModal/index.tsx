import { Delete } from "@mui/icons-material";
import { Box, Button, TextField, Typography } from "@mui/material";
import Modal from '@mui/material/Modal';
import GoalStatus from "enums/GoalStatus";
import { deleteById, getAllGoals } from "hooks/global/reducers/goalSlice";
import store from "global/store";
import IGoal from "interfaces/IGoal";
import { useState } from "react";

type Props = {
  goal: IGoal
}

const DeleteGoalModal: React.FC<Props> = (props: Props) => {
  const [isOpen, setIsOpen] = useState(false)
  const [confirm, setConfirm] = useState<string>("");

  const handlerOpenAndClose = () => setIsOpen(!isOpen);

  function handlerDelete() {
    if ((props.goal === undefined) ||
      (props.goal.title.length === 0) ||
      (props.goal.description.length === 0) ||
      (props.goal.date === undefined) ||
      (props.goal.status === undefined) ||
      (confirm !== props.goal.title)
    ) return;

    store.dispatch(deleteById(props.goal.id));
    setTimeout(() => {
      store.dispatch(getAllGoals());
      handlerOpenAndClose();
    }, 1000);

  }

  return (
    <>
      <Button
        variant="contained"
        color="error"
        onClick={handlerOpenAndClose}
      >
        <Delete />
      </Button>
      <Modal
        open={isOpen}
        onClose={handlerOpenAndClose}
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        sx={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <Box
          component="div"
          sx={{
            position: 'absolute' as 'absolute',
            display: 'flex',
            flexDirection: 'column',
            width: 400,
            p: 2,
            bgcolor: '#fff',
            gap: '16px',
          }}
        >
          <Typography
            component="h3"
            variant="h5"
            sx={{
              fontWeight: 'bold',
              textAlign: 'center'
            }}
          >
            Excluir Tarefa
          </Typography>
          <Box component="ul">
            <Box component="li">
              <Typography component="p" fontWeight="bold">
                Título: {props.goal.title}
              </Typography>
            </Box>
            <Box component="li">
              <Typography component="p" fontWeight="bold">
                Descrição: {props.goal.description}
              </Typography>
            </Box>
            <Box component="li">
              <Typography component="p" fontWeight="bold">
                Data: {new Date(props.goal.date || 0).toLocaleDateString("pt-br")}
              </Typography>
            </Box>
            <Box component="li">
              <Typography component="p" fontWeight="bold">
                Status: {Object.values(GoalStatus)[props.goal.status]}
              </Typography>
            </Box>
          </Box>
          <Typography component="p">
            Digite o titulo da tarefa selecionada para confirmar a exclusão:
          </Typography>
          <TextField
            label="Confirmar"
            size="small"
            value={confirm}
            placeholder="Confirme o Título"
            onChange={e => setConfirm(e.currentTarget.value)}
          >
          </TextField>

          <Button onClick={handlerDelete} color="error" variant="contained" size="large">
            Excluir
            <Delete />
          </Button>
        </Box>
      </Modal>
    </>
  );
}

export default DeleteGoalModal;
import { yupResolver } from "@hookform/resolvers/yup";
import { Edit } from "@mui/icons-material";
import { Box, Button, FormControl, FormHelperText, InputLabel, MenuItem, Select, TextField, Typography } from "@mui/material"
import Modal from '@mui/material/Modal';
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { convertStatusToEnum, convertStatusToString } from "configs/helpers";
import GoalStatus from "enums/GoalStatus";
import { update, getAllGoals } from "hooks/global/reducers/goalSlice";
import store from "global/store";
import goalFormSchema from "hooks/validations/goalSchema";
import IGoal from "interfaces/IGoal";
import { useState } from "react";
import { Controller, useForm } from "react-hook-form";

type Props = {
  goal: IGoal;
}

const UpdateGoalModal: React.FC<Props> = (props: Props) => {
  const [isOpen, setIsOpen] = useState(false)

  const { control, handleSubmit, reset, setValue } = useForm<IGoal>({
    resolver: yupResolver(goalFormSchema),
  });

  const handlerOpenAndClose = () => {
    setValue("date", props.goal.date);
    setValue("description", props.goal.description);
    setValue("status", props.goal.status);
    setValue("title", props.goal.title);
    setValue("id", props.goal.id);

    setIsOpen(!isOpen);
  };




  function handlerOnSubmit(data: IGoal) {

    store.dispatch(update(data));

    setTimeout(() => {
      store.dispatch(getAllGoals());
      handlerOpenAndClose();
    }, 1000);   

    reset()
  }

  return (
    <>
      <Button
        variant="contained"
        onClick={handlerOpenAndClose}
      >
        <Edit />
      </Button>
      <Modal
        open={isOpen}
        onClose={handlerOpenAndClose}
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        sx={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <Box
          component="form"
          onSubmit={handleSubmit(handlerOnSubmit)}
          sx={{
            position: 'absolute' as 'absolute',
            display: 'flex',
            flexDirection: 'column',
            width: 400,
            p: 2,
            bgcolor: '#fff',
            gap: '16px',
          }}
        >
          <Typography
            component="h3"
            variant="h5"
            sx={{
              fontWeight: 'bold',
              textAlign: 'center'
            }}
          >
            Criar Tarefa
          </Typography>

          <Controller
            name="title"
            control={control}
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <TextField
                label="Título"
                size="small"
                value={value}
                onChange={onChange}
                error={!!error}
                helperText={error ? error.message : null}
              />
            )}
          />
          <Controller
            name="description"
            control={control}
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <TextField
                label="Descrição"
                size="small"
                placeholder="Descreva a tarefa"
                value={value}
                error={!!error}
                onChange={onChange}
                helperText={error?.message}
              />
            )}
          />
          <Controller
            name="date"
            control={control}
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <LocalizationProvider dateAdapter={AdapterDayjs} >
                <DesktopDatePicker
                  label="Data"
                  inputFormat="DD/MM/YYYY"
                  value={value}
                  onChange={onChange}
                  renderInput={(params) => <TextField {...params} />}
                />
                <FormHelperText color="#f00">
                  {error?.message}
                </FormHelperText>
              </LocalizationProvider>
            )}
          />
          <Controller
            name="status"
            control={control}
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <FormControl>
                <InputLabel id="select-label-goal-status">Status</InputLabel>
                <Select
                  aria-label=""
                  labelId="select-label-goal-status"
                  size="small"
                  value={value}
                  error={!!error}
                  onChange={onChange}>
                  <MenuItem value={convertStatusToEnum(GoalStatus.Pendente)}>
                    {convertStatusToString(GoalStatus.Pendente)}
                  </MenuItem>
                  <MenuItem value={convertStatusToEnum(GoalStatus.Finalizado)}>
                    {convertStatusToString(GoalStatus.Finalizado)}
                  </MenuItem>
                </Select>
                <FormHelperText color="error">
                  {error?.message}
                </FormHelperText>
              </FormControl>
            )}
          />

          <Button type="submit" variant="contained" size="large">
            Editar
          </Button>
        </Box>
      </Modal>
    </>
  );
}

export default UpdateGoalModal;
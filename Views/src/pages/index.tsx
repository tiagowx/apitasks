import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from "./components/Layout";
import ServiceOrderForm from "./components/ServiceOrderForm";
import Home from "./Home";


const Pages: React.FC = () => {


  return (
    <Layout>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/testes" element={<ServiceOrderForm />} />
        </Routes>
      </BrowserRouter>
    </Layout>
  );
}

export default Pages;

import { Search, Update } from "@mui/icons-material";
import { Dayjs } from "dayjs";
import { Box, Button, MenuItem, Select, TextField } from "@mui/material";
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import IGoal from "interfaces/IGoal";
import GoalsTable from "pages/components/GoalsTable";
import React, { useEffect, useState } from "react";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import CreateGoalModal from "pages/components/CreateGoalModal";
import { useSelector } from "react-redux";
import { getAllGoals, getByFilters, selectAllGoals } from 'hooks/global/reducers/goalSlice'
import store from "global/store";
import { convertStatusToEnum, convertStatusToString } from "configs/helpers";
import GoalStatus from "enums/GoalStatus";

const Home: React.FC = () => {
  const goals: IGoal[] = useSelector(selectAllGoals);
  const [search, setSearch] = useState<string>("");
  const [date, setDate] = useState<Dayjs | null>(null);
  const [goal, setGoal] = useState<IGoal>({
    id: 0,
    title: "",
    description: "",
    status: 0,
    date: new Date(),
  });

  async function handlerSearch(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    if (search.length <= 0) {
      console.log(`Campo vazio ${search.valueOf}`);
      return;
    }

    store.dispatch(getByFilters(goal));
  }

  useEffect(() => {
    if ((goals.length !== store.getState().goals.length) || goals.length === 0) {
      setTimeout(() => {
        store.dispatch(getAllGoals());
        console.log(store.getState().goals.length);
      }, 3000);
    }
  }, [goals]);

  return (
    <Box
      component="section"
      sx={{
        maxHeight: '100%'
      }}>
      <Box
        component="form"
        onSubmit={handlerSearch}
        sx={{
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <TextField
          size="small"
          value={search}
          onChange={e => setSearch(e.currentTarget.value)}>
        </TextField>
        <Button type="submit" variant="contained">
          <Search />
        </Button>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <DatePicker
            label="Data"
            value={date}
            onChange={(newValue) => {
              setGoal({
                ...goal,
                date: new Date(newValue?.toString() || new Date(0))
              })
            }}
            renderInput={(params) => <TextField size="small" {...params} />}
          />
        </LocalizationProvider>
        <Select
          label="status"
          size="small"
          value={goal.status}
          onChange={e =>
            setGoal({
              ...goal,
              status: convertStatusToEnum(e.target.value)
            })
          }>
          <MenuItem value={GoalStatus.Pendente}>
            {convertStatusToString(GoalStatus.Pendente)}
          </MenuItem>
          <MenuItem value={GoalStatus.Finalizado}>
            {convertStatusToString(GoalStatus.Finalizado)}
          </MenuItem>
        </Select>
      </Box>
      <Box sx={{
        display: 'flex',
        gap: '8px',
        pl: '8px'

      }}>
        <CreateGoalModal />
        <Button variant="outlined" onClick={() => store.dispatch(getAllGoals())}>
          <Update />
        </Button>
      </Box>
      <GoalsTable goals={goals} />
    </Box>
  );
}

export default Home;
using apitasks.Models;
using System.ComponentModel.DataAnnotations;

namespace apitasks.Models
{
    public class Goal
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        public EnumStatusGoal Status { get; set; }
    }
}
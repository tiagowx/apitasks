using Microsoft.EntityFrameworkCore;
using apitasks.Models;

namespace apitasks.Context
{
    public class ApiContext: DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options) : base(options)
        {
            
        }

        public DbSet<Goal> Goals { get; set; }
    }
}